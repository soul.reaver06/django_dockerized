#!/bin/sh

apt-get update
apt-get install docker
curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
docker-compose --version
apt-get install git

if [ $1 == "prod" ]; then
    mkdir /var/$1
    cd /var/$1
    git clone -b master  https://soul.reaver06:dddev911@gitlab.com/soul.reaver06/django_dockerized.git
    cd django_dockerized
    docker-compose -f docker-compose.prod.yml up -d --build

else    
    mkdir /var/$1
    cd /var/$1
    git clone -b develop https://soul.reaver06:dddev911@gitlab.com/soul.reaver06/django_dockerized.git
    cd django_dockerized
    docker-compose -f docker-compose.yml up -d --build
    
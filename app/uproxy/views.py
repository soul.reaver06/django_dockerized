import json
from django.http import JsonResponse
from django.shortcuts import render
import requests
import uproxy.config as lconfig
import logging


# Get an instance of a logger

logger = logging.getLogger("main_app")


def chatproxy(request):
    # division_by_zero = 1 / 0
    # DEBUG
    logger.debug('Email to user id={} sent')

    # INFO
    # logger.info('Payment transaction finished with status={}')

    # WARNING
    # logger.error('Referer {} is not in allow list')
    context = {'port': lconfig.PORT}

    return render(request, "chat_proxy.html", context)


def getresp(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        # return data['msg']
        if "msg" not in data:
            resp = {
                "status": 'error',
                "msg": 'Empty msg input - 007-A',
            }

        if "additional" not in data:
            resp = {
                "status": 'error',
                "msg": 'Empty additional info missing - 007-A',
            }

        if data['env'] == "pre":
            url = 'https://jio-enp.hellohaptik.com/integration/hlts/umang_prestaging/'
        elif data['env'] == "super":
            url = 'https://jio-enp.hellohaptik.com/integration/hlts/umang_staging_superman/'
        elif data['env'] == "presuper":
            url = 'https://jio-prestaging.hellohaptik.com/integration/hlts/umang_staging_superman/'

        else:
            url = 'https://jio-enp.hellohaptik.com/integration/hlts/umang_haptik/'

        print(url)

        payload = {
            "event": "message",
            "auth_id": data['user_id'],
            "message": data['msg'],
            "language": data['lang'],
            "lat": "",
            "long": "",
            "os_version": "mac",
            "package_name": "web",
            "debug": "true"
        }

        # Adding empty header as parameters are being sent in payload
        headers = {
            "Content-Type": "application/json",
        }
        print(payload)
        try:
            r = requests.post(url, data=json.dumps(payload), headers=headers, timeout=30)
        except(Exception):
            resp = {
                "status": 'failed',
                "msg": "Unable to connect AI server - 007 B",
                "additional": data['additional'],
            }

        json_data = json.loads(r.content)
        print(json_data)

        if not json_data:
            resp = {
                "status": 'failed',
                "msg": "Unable to process request - 007 B",
                "additional": data['additional'],
            }
        if json_data['error']:
            resp = {
                "status": 'failed',
                "msg": json_data['error'] + '007 C',
                "additional": data['additional'],
            }

        # print(json_data['body'][0]['web_response']['notification_text'])
        try:
            resp = {
                "status": 'sucess',
                "msg": json_data['body'][0]['web_response']['notification_text'],
                "additional": data,
                "payload": json_data
            }
        except(Exception):
            resp = {
                "status": 'sucess',
                "msg": "data issue check inspect",
                "additional": data,
                "payload": json_data
            }
    else:
        resp = {
            "status": 'error',
            "msg": 'Failed to send to the AI-007',
        }
    return JsonResponse(resp)
